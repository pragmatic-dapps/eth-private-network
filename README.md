**Master credentials:**

`pragmaticdlt`
`password`

**Network info:**

wallet-stats-faucet-node-16 40.85.211.53 - `ssh pragmaticdlt@40.85.211.53`

boot-node-us-16 40.67.176.8 - `ssh pragmaticdlt@40.67.176.8`

seal-1-node-us-16 40.67.176.32 - `ssh pragmaticdlt@40.67.176.32`

seal-2-node-us-16 40.67.176.242 - `ssh pragmaticdlt@40.67.176.242`

seal-3-node-us-16 40.67.177.6 - `ssh pragmaticdlt@40.67.177.6`

boot-node-eu-16 40.67.176.8 - `ssh pragmaticdlt@40.67.176.8`

seal-1-node-eu-16 40.67.176.32 - `ssh pragmaticdlt@40.67.176.32`

seal-2-node-eu-16 40.67.176.242 - `ssh pragmaticdlt@40.67.176.242`

seal-3-node-eu-16 40.67.177.6 - `ssh pragmaticdlt@40.67.177.6`


**Setup manual:**

0. Dir structure:

~/go-ethereum
~/supplychain/supplychain.json
~/supplychain/bootnode/keystore
~/supplychain/seal-node-1/keystore
~/supplychain/seal-node-2/keystore
~/supplychain/seal-node-3/keystore
~/supplychain/ethstats

1. Install go:

`curl https://dl.google.com/go/go1.11.linux-amd64.tar.gz > go1.11.linux-amd64.tar.gz`

`sudo tar -C /usr/local -xzf go1.11.linux-amd64.tar.gz`

`mkdir ~/.go`

add 
`GOROOT=/usr/local/go
GOPATH=~/.go
PATH=$PATH:$GOROOT/bin:$GOPATH/bin
`
to `/etc/profile`

`sudo update-alternatives --install "/usr/bin/go" "go" "/usr/local/go/bin/go" 0`
`sudo update-alternatives --set go /usr/local/go/bin/go`

`go version`

2. Download and build go-ethereum:
`sudo apt-get update`

`sudo apt-get install -y build-essential`

`git clone https://github.com/ethereum/go-ethereum`

`cd go-ethereum` 
`git tag -l`
`git checkout tags/v1.8.16`

`make all`

3. Setup seal-node-n (`passphrase - 1qaz2wsx3edc`):
   
`mkdir -p ~/supplychain/seal-node-n`
   
`~/go-ethereum/build/bin/geth --datadir seal-node-n/ account new` -> `fbf77480a8b01b23d70257bc0d3a76d6369a74d4`
 

5. Create `genesis.json` using puppeth

`~/go-ethereum/build/bin/puppeth`

6. Init seal-node-n:

`~/go-ethereum/build/bin/geth --datadir seal-node-n/ init supplychain.json`


8. Run boot-node:

`~/go-ethereum/build/bin/geth --datadir bootnode init supplychain.json`

`~/go-ethereum/build/bin/bootnode -genkey boot.key`

`~/go-ethereum/build/bin/bootnode -nodekey boot.key -verbosity 9 -addr :30310`


8. Run seal-node-n:
write pass to `seal-node-n.pass`
`~/go-ethereum/build/bin/geth --datadir seal-node-n/ --syncmode 'full' --port 30311 --rpc --rpcaddr '0.0.0.0' --rpcport 8501 --rpcapi 'personal,db,eth,net,web3,txpool,miner' --bootnodes 'enode://5319459cec9a0cc1ac511aef7c716b95a9789f424c387acbfb8de5584b3e74c3736d3dc5b00c69f8ecaee8997ebe55249c0a0a0dd1894561e63024975ec3730b@40.67.176.8:30310' --networkid 1605 --gasprice '1' -unlock '0xfbf77480a8b01b23d70257bc0d3a76d6369a74d4' --password ./seal-node-n.pass --mine  --ethstats seal-node-n-us:1qaz2wsx3edc@40.85.211.53:8081 --targetgaslimit 94000000`


-----------OTHER----------
1. install docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-get install docker-compose
sudo usermod -aG docker $USER
relogin

2. copy from remote machine
scp -r pragmaticdlt@40.85.211.53:~/supplychain.json .

3. connect to geth console running in docker
docker exec -it 33f5225ec3a6 geth attach ipc:/root/.ethereum/geth.ipc

4. attach to geth console:
~/go-ethereum/build/bin/geth attach ipc:seal-node-n/geth.ipc

5. manage accounts
personal.newAccount()
personal.unlockAccount(address, "password")

